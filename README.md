retrostrings
============

Blitz Basic styled string functionality for the [Monkey programming language](https://github.com/blitz-research/monkey).

This module was originally released in 2013; that release can be found here: http://www.monkey-x.com/Community/posts.php?topic=4831

There are no *major* differences between this version, and the original version on Google Code. (It is still recommended that you get this version over that, though)